﻿using UnityEngine;
using System.Collections;

public class RogueAIScript : MonoBehaviour {

    public bool SeekEnabled = true;
    public bool flipEnabled = false;
    public bool ShootEnabled = false;
    public bool ChargeEnabled = false;
    public bool instabilityEnabled = false;
    public bool RandomizeScaleEnabled = false;
    public bool RandomizeColorEnabled = false;
    public bool RandomizeSpeedEnabled = false;
    public bool RandomizeBonusEnabled = false;
    public bool hasBonus = false;
    public bool isEsential = true;
    public int killValue = 1;
    public GameObject[] bonus;
    
    private RogueMoveScript move;
    private RogueNewWeaponScript weapon;
    private GameObject player;

    private Vector3 wanderTargetPos;

    private float next_action = 0.0f;

    private bool lookRight = false;

    private float charge_rate = 2f;
    private float charge_duration = 1.0f;
    private float charge_speed = 3.0f;
    
    void Awake()
    {
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        player = GameObject.FindGameObjectsWithTag("Player")[0];
    }

    void Start () {
        move = GetComponentInChildren<RogueMoveScript>();
        weapon = GetComponentInChildren<RogueNewWeaponScript>();
        wanderTargetPos = UnityEngine.Random.insideUnitSphere * 5;

        if (RandomizeScaleEnabled)
            RandomizeScale();
        if (RandomizeColorEnabled)
            RandomizeColor();
        if (RandomizeSpeedEnabled)
            RandomizeSpeed();
        if (RandomizeBonusEnabled)
            RandomizeBonus();
    }
    
    void Update () {
        if (SeekEnabled)
            Seek();
        if (flipEnabled)
            Flip();
        if (ChargeEnabled)
            Charge();
        if (ShootEnabled)
            Shoot();
    }
    
    void Kill()
    {
        if (bonus.Length > 0 && hasBonus)
        {
            var instance = Instantiate(bonus[Random.Range(0, bonus.Length)]) as GameObject;
            instance.transform.position = transform.position;
        }
        if (GetComponentInParent<RogueMapGenerator>() != null)
            GetComponentInParent<RogueMapGenerator>().monstersLeft -= 1;
        FindObjectOfType<RogueMapGenerator>().BroadcastMessage("CheckLeftEnemies");
        Destroy(gameObject);
    }


    #region Integration

    private void Seek()
    {
        if (player == null)
            return;
        Vector3 target = player.transform.position;
        Vector3 pos = transform.position;
        Vector3 vectorDirector = (target - pos).normalized;
        transform.position += vectorDirector * move.moveSpeed * Time.deltaTime;
    }
    
    public void Flip()
    {
    }

    private void Shoot()
    {
        weapon.EnemyShoot(player.transform.position);
    }
    
    private void Charge()
    {
        if (Time.time > next_action)
        {
            move.moveSpeed += charge_speed;
            next_action = Time.time + charge_rate;
            if (Time.time > charge_duration)
            {
                move.moveSpeed -= charge_speed;
                next_action = Time.time + charge_rate;
            }
        }
    }

    private void RandomizeScale()
    {
        //Random scale
        float newScale = Random.Range(-0.3f, 0.3f);
        transform.localScale += new Vector3(newScale, newScale);
    }

    private void RandomizeColor()
    {
        //Random color
        Color newColor = new Color(Random.value, Random.value, Random.value, 1.0f);
        GetComponent<SpriteRenderer>().color = newColor;
    }

    private void RandomizeSpeed()
    {
        //Random speed
        float newSpeed = Random.Range(-0.3f, 1.0f);
        move.moveSpeed += newSpeed;
    }

    private void RandomizeBonus()
    {
        //Random speed
        this.hasBonus = GenerateRandomBool(10);
    }

    #endregion

    #region Utils

    private bool GenerateRandomBool(int chance_of_success)
    {
        int random = Random.Range(0, 100);
        if (random <= chance_of_success)
            return true;
        else
            return false;
    }

    #endregion
}
