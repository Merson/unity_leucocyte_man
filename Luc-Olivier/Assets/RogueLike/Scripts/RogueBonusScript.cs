﻿using UnityEngine;
using System.Collections;

public class RogueBonusScript : MonoBehaviour
{

    private bool isHealBonus = false;            public float healForce = 1;
    private bool isAttackPowerBonus = false;     public float AttackPowerForce = 0.5f;
    private bool isRangedBonus = false;          public float RangeForce = 0.5f;
    private bool isSpeedBonus = false;           public float SpeedForce = 0.5f;
    private bool isAttackSpeedBonus = false;     public float AttackSpeedForce = 0.10f;
    private bool isBulletSpeedBonus = false;     public float BulletSpeedForce = 2f;

    public Sprite heal;
    public Sprite attack;
    public Sprite range;
    public Sprite speed;
    public Sprite attack_speed;
    public Sprite bullet_speed;

    void Start()
    {
        int i;
        i = Random.Range(0, 8);
        if (i == 0)
            isSpeedBonus = true;
        else if (i == 1)
            isRangedBonus = true;
        else if (i == 2)
            isAttackPowerBonus = true;
        else if (i == 3)
            isAttackSpeedBonus = true;
        else if (i == 4)
            isBulletSpeedBonus = true;
        else
            isHealBonus = true;

        if (isHealBonus && heal != null)
        {
            GetComponent<SpriteRenderer>().sprite = heal;
        }
        else if (isAttackPowerBonus && attack != null)
        {
            GetComponent<SpriteRenderer>().sprite = attack;
        }
        else if (isRangedBonus && range != null)
        {
            GetComponent<SpriteRenderer>().sprite = range;
        }
        else if (isSpeedBonus && speed != null)
        {
            GetComponent<SpriteRenderer>().sprite = speed;
        }
        else if (isAttackSpeedBonus && attack_speed != null)
        {
            GetComponent<SpriteRenderer>().sprite = attack_speed;
        }
        else if (isBulletSpeedBonus && bullet_speed != null)
        {
            GetComponent<SpriteRenderer>().sprite = bullet_speed;
        }
        Destroy(gameObject, 22);
    }

    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            GetComponent<RogueAudioHelper>().PlayWeaponSound();

            if (isHealBonus)
            {
                collider.GetComponent<RogueHealthScript>().hp += healForce;
            }
            else if (isAttackPowerBonus)
            {
                collider.GetComponentInChildren<RogueNewWeaponScript>().shootPrefabPower += AttackPowerForce;
            }
            else if (isRangedBonus)
            {
                collider.GetComponentInChildren<RogueNewWeaponScript>().shootPrefabLifeTime += RangeForce;
            }
            else if (isSpeedBonus)
            {
                collider.GetComponent<RoguePlayerScript>().moveSpeed += SpeedForce;
            }
            else if (isAttackSpeedBonus)
            {
                collider.GetComponentInChildren<RogueNewWeaponScript>().shootingRate /= 2;
            }
            else if (isBulletSpeedBonus)
            {
                collider.GetComponentInChildren<RogueNewWeaponScript>().shootPrefabSpeed += BulletSpeedForce;
            }
            collider.GetComponent<RoguePlayerScript>().refreshText();
            Destroy(gameObject);
        }
    }
}
