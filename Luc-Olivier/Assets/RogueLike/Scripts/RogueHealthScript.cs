﻿using UnityEngine;
using System.Collections;

public class RogueHealthScript : MonoBehaviour
{
    public Transform[] enemyPrefabs;
    public float hp = 1;
    public bool isEnemy = true;

    void OnCollisionEnter2D(Collision2D collider)
    {
        if (!isEnemy)
        {
            if (collider.gameObject.tag == "Monster")
            {
                hp -= 1;
                if (collider.gameObject.GetComponent<RoguePlayerScript>() != null)
                    collider.gameObject.GetComponent<RoguePlayerScript>().refreshText();
            }
        }
        else if (isEnemy)
        {
            if (collider.gameObject.tag == "Player") {
                if (GetComponent<RogueAudioHelper>() != null)
                {
                    GetComponent<RogueAudioHelper>().PlayCollisionSound();
                }
            }
        }
        if (collider.gameObject.GetComponent<RogueAIScript>() != null)
            CheckHp(collider.gameObject.GetComponent<RogueAIScript>().isEsential);
        else
            CheckHp(false);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        RogueShootScript shoot = collider.gameObject.GetComponent<RogueShootScript>();

        if (!isEnemy)
        {
            if (collider.gameObject.tag == "Projectile")
            {
                if (!shoot.isPlayer)
                {
                    hp -= shoot.shootPower;
                    if (collider.gameObject.GetComponent<RoguePlayerScript>() != null)
                        collider.gameObject.GetComponent<RoguePlayerScript>().refreshText();
                }
            }
        }
        else if (isEnemy)
        {
            if (collider.gameObject.tag == "Projectile")
            {
                if (shoot.isPlayer)
                    hp -= shoot.shootPower;
            }
        }
        if (collider.gameObject.GetComponent<RogueAIScript>() != null)
        {
            bool tmp = collider.gameObject.GetComponent<RogueAIScript>().isEsential;
            CheckHp(tmp);
        }
        else
            CheckHp(false);
    }

    private void CheckHp(bool isEssential)
    {
        if (hp <= 0)
        {

            
             print("ENEMY ESSENTIAL = " + isEssential);
            if (isEnemy)
            {
               BroadcastMessage("Kill");
            }
            else
            {
                SceneLoader test = new SceneLoader();
                test.Load("Scene1");
            }
        }
    }
}
