﻿using UnityEngine;
using System.Collections;

public class RogueNewWeaponScript : MonoBehaviour {

    public string fireButton = "Fire1";

    public bool isPlayer = false;

    public Vector3 shootPrefabScale = new Vector3(1, 1, 1);
    public Vector3 shootPrefabRotation = new Vector3(0, 0, 0);

    public float shootingRate = 0.50f;
    public float shootPrefabPower = 1.0f;
    public float shootPrefabSpeed = 3.0f;
    public float shootPrefabLifeTime = 5.0f;
    public float shootForce = 0.0f;
    public float shootPrecision = 1.0f;
    public float shootRange = 2.0f;

    public GameObject shootPrefab;
    
    private float shootCooldown;
    
    void Start () {
        shootCooldown = 0.0f;
    }
	
	void Update () {
        if (shootCooldown > 0)
            {
                shootCooldown -= Time.deltaTime;
            }
        /*if (Input.GetButtonDown(fireButton))
        {
            RogueNewWeaponScript weapon = GetComponent<RogueNewWeaponScript>();
            if (weapon != null && isPlayer == true && shootPrefab != null)
                weapon.PlayerShoot();
        }*/
        if (Input.GetButtonDown(fireButton))
        {
            RogueNewWeaponScript weapon = GetComponent<RogueNewWeaponScript>();
            if (weapon != null && isPlayer == true && shootPrefab != null)
                InvokeRepeating("PlayerShoot", .001f, shootingRate);
        }
        else if (Input.GetButtonUp(fireButton))
        {
            CancelInvoke("PlayerShoot");
        }
    }

    private void launchProjectile(bool isEnemyShot, Vector3 VectorDirector)
    {

        GameObject toInstance = new GameObject();
        toInstance = Instantiate(shootPrefab, transform.position, Quaternion.identity) as GameObject;
        //toInstance.transform.localScale = shootPrefabScale;
        if (toInstance.GetComponent<Rigidbody2D>() != null)
        {
            toInstance.GetComponent<Rigidbody2D>().velocity = VectorDirector * shootPrefabSpeed;
            toInstance.GetComponent<Rigidbody2D>().AddForce(VectorDirector * shootForce);
        }
        else
        {
            toInstance.AddComponent<RogueShootScript>();
        }
        RogueShootScript tmp = toInstance.AddComponent<RogueShootScript>();
        tmp.setShoot(this.isPlayer, this.shootPrefabPower, this.shootPrefabLifeTime, this.shootPrefabRotation);

        if (GetComponentInParent<RogueAudioHelper>() != null) {
            if (GetComponentInParent<RogueAudioHelper>().weaponSounds != null)
                GetComponentInParent<RogueAudioHelper>().PlayWeaponSound();
        }
    }

    private void PlayerShoot()
    {
        if (canShoot)
        {
            shootCooldown = shootingRate;
            Vector3 target = new Vector3(Random.Range(Input.mousePosition.x - shootPrecision, Input.mousePosition.x + shootPrecision), Random.Range(Input.mousePosition.y - shootPrecision, Input.mousePosition.y + shootPrecision));
            Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
            Vector3 vectorDirector = (target - pos).normalized;
            launchProjectile(false, vectorDirector);
        }
    }

    public void EnemyShoot(Vector3 position)
    {
        if (canShoot)
        {
            shootCooldown = shootingRate;
            Vector3 target = new Vector3(Random.Range(position.x - shootPrecision, position.x + shootPrecision), Random.Range(position.y - shootPrecision, position.y + shootPrecision));
            Vector3 pos = transform.position;
            Vector3 vectorDirector = (target - pos).normalized;
            launchProjectile(true, vectorDirector);
        }
    }

    private bool canShoot
    {
        get
        {
            return shootCooldown <= 0f;
        }
    }

}
