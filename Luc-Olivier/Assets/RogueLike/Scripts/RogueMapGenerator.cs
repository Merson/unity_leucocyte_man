﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

public class RogueMapGenerator : MonoBehaviour {

    public string map;
    private string path;
    public int monstersLeft = 0;

    public const string isBonusHealth = "5";
    public const string isNothing = " ";
    public const string isFlesh1 = "0";
    public const string isFlesh2 = "1";
    public const string isFlesh3 = "2";
    public const string isFlesh4 = "3";
    public const string isPlayer1 = "p";
    public const string isEnemy1 = "x";
    public const string isEnemy2 = "y";
    public const string isEnemyGenerator = "g";
    public const string isInfectionLevel1 = "h";
    public const string isInfection1 = "i";
    public const string isInfection2 = "j";
    public const string isBoss1 = "b";
    public const string isBoss2 = "c";
    public const string isBoss3 = "d";
    public const string isMycose = "M";
    public const string isBadBloodCell = "B";


    public GameObject BonusHealth;
    public GameObject Flesh1;
    public GameObject Flesh2;
    public GameObject Flesh3;
    public GameObject Flesh4;
    public GameObject Player1;
    public GameObject Enemy1;
    public GameObject Enemy2;
    public GameObject EnemyGenerator;
    public GameObject infectionLevel1;
    public GameObject infection1;
    public GameObject infection2;
    public GameObject boss1;
    public GameObject boss2;
    public GameObject boss3;
    public GameObject mycose;
    public GameObject BadBloodCell;
    public string nextScene = "";



    private void Awake()
    {
        path = Application.dataPath + "/RogueLike" + "/Maps/" + map;
        print(path);
    }

    private string[][] ReadFile(string file)
    {
        string text = System.IO.File.ReadAllText(file);
        string[] lines = Regex.Split(text, "\r\n");
        int rows = lines.Length;
        string[][] levelBase = new string[rows][];
        for (int i = 0; i < lines.Length; i++)
        {
            string[] stringsOfLine = Regex.Split(lines[i], " ");
            levelBase[i] = stringsOfLine;
        }
        return levelBase;
    }

    private void GenerateMap(string[][] map)
    {
        for (int y = 0; y < map.Length; y++)
        {
            for (int x = 0; x < map[y].Length; x++)
            {
                GameObject toInstantiate = null;
                GameObject ground = null;
                switch (map[y][x])
                {
                    case isFlesh2:
                        toInstantiate = Flesh2;
                        break;
                    case isFlesh3:
                        toInstantiate = Flesh3;
                        break;
                    case isFlesh4:
                        toInstantiate = Flesh4;
                        break;
                    case isPlayer1:
                        toInstantiate = Player1;
                        break;
                    default:
                        break;
                }
                if (toInstantiate != null)
                {
                    GameObject instance = Instantiate(toInstantiate, new Vector3((x * 1), (y * 1), 0f), Quaternion.identity) as GameObject;
                    instance.transform.SetParent(transform.GetChild(0));
                }
            }
        }
    }

    private void GenerateBonus(string[][] map)
    {
        for (int y = 0; y < map.Length; y++)
        {
            for (int x = 0; x < map[y].Length; x++)
            {
                GameObject toInstantiate = null;
                switch (map[y][x])
                {
                    case isBonusHealth:
                        toInstantiate = BonusHealth;
                        break;

                }
                if (toInstantiate != null)
                {
                    GameObject instance = Instantiate(toInstantiate, new Vector3((x * 1), (y * 1), 0f), Quaternion.identity) as GameObject;
                    instance.transform.SetParent(transform.GetChild(0));
                }
            }
        }
    }
    
    private void GenerateEnemies(string[][] map)
    {
        for (int y = 0; y < map.Length; y++)
        {
            for (int x = 0; x < map[y].Length; x++)
            {
                GameObject toInstantiate = null;
                switch (map[y][x])
                {
                    case isInfectionLevel1:
                        toInstantiate = infectionLevel1;
                        monstersLeft += 1;
                        break;
                    case isInfection1:
                        toInstantiate = infection1;
                        monstersLeft += 1;
                        break;
                    case isInfection2:
                        toInstantiate = infection2;
                        monstersLeft += 1;
                        break;
                    case isBoss1:
                        toInstantiate = boss1;
                        monstersLeft += 1;
                        break;
                    case isBoss2:
                        toInstantiate = boss2;
                        monstersLeft += 1;
                        break;
                    case isBoss3:
                        toInstantiate = boss3;
                        monstersLeft += 1;
                        break;
                    case isMycose:
                        toInstantiate = mycose;
                        monstersLeft += 1;
                        break;
                    case isBadBloodCell:
                        toInstantiate = BadBloodCell;
                        monstersLeft += 1;
                        break;
                }
                if (toInstantiate != null)
                {
                    GameObject instance = Instantiate(toInstantiate, new Vector3((x * 1), (y * 1), 0f), Quaternion.identity) as GameObject;
                    instance.transform.SetParent(transform.GetChild(0));
                }
            }
        }
    }
    
    void Start()
    {
        string[][] map = ReadFile(path);
        GenerateMap(map);
        GenerateBonus(map);
        GenerateEnemies(map);
    }

    public void CheckLeftEnemies()
    {
        FindObjectOfType<RoguePlayerScript>().refreshText();
        if (monstersLeft <= 0)
        {
            GameObject Player1Save = Player1;
            SceneLoader test = new SceneLoader();

            if (nextScene != "")
                test.Load(nextScene);
            else
                test.Load("Scene1");
        }
    }

    public void exit()
    {
        Application.Quit();
    }

    void Update ()
    {
	
	}
}
