﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
  public void Load(string scene)
  {
    SceneManager.LoadScene(scene);
  }
  public static void StaticLoad(string scene)
  {
    SceneManager.LoadScene(scene);
  }
    public void exit()
    {
        Application.Quit();
    }

}