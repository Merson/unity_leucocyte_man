﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RoguePlayerScript : MonoBehaviour {

    public Text AttackText;
    public Text HealthText;
    public Text SpeedText;
    public Text RangeText;
    public Text EnemiesLeftText;

    public float moveSpeed = 6.0f;
    private Vector2 move = Vector2.zero;
    private Rigidbody2D rb2d;
    
	void Start () {
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        AttackText.text = "Attack: " + GetComponentInChildren<RogueNewWeaponScript>().shootPrefabPower.ToString();
        SpeedText.text = "Speed: " + moveSpeed.ToString();
        RangeText.text = "Range: " + GetComponentInChildren<RogueNewWeaponScript>().shootPrefabLifeTime.ToString();
        HealthText.text = "Health: " + GetComponent<RogueHealthScript>().hp.ToString();
        EnemiesLeftText.text = "Enemies left: " + FindObjectOfType<RogueMapGenerator>().monstersLeft.ToString();
    }

    public void refreshText()
    {
        AttackText.text = "Attack: " + GetComponentInChildren<RogueNewWeaponScript>().shootPrefabPower.ToString();
        SpeedText.text = "Speed: " + moveSpeed.ToString();
        RangeText.text = "Range: " + GetComponentInChildren<RogueNewWeaponScript>().shootPrefabLifeTime.ToString();
        HealthText.text = "Health: " + GetComponent<RogueHealthScript>().hp.ToString();
        EnemiesLeftText.text = "Enemies left: " + FindObjectOfType<RogueMapGenerator>().monstersLeft.ToString();
    }

    void Update () {
        float inputX = Input.GetAxis("Horizontal");
        float inputY = Input.GetAxis("Vertical");
        move = new Vector2(moveSpeed * inputX, moveSpeed * inputY);

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneLoader test = new SceneLoader();
            test.Load("Scene1");
        }
    }

    void FixedUpdate()
    {
        GetComponent<Rigidbody2D>().velocity = move;
    }
}
