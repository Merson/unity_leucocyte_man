﻿using UnityEngine;
using System.Collections;

public class RogueMoveScript : MonoBehaviour
{

    public float moveSpeed = 1.0f;
    public float rotationSpeed = 1.0f;
    public float rotation = 1.0f;

    private Rigidbody2D rgbd;
    private GameObject player;

    public void instanciate(Vector3 direction)
    {
        rgbd = GetComponent<Rigidbody2D>();
        rgbd.velocity = direction * moveSpeed;
    }
    
    void Start()
    {
        player = GameObject.Find("Player1");
    }

    void Update()
    {
        if (gameObject.tag == "Monster")
        {
            //transform.position += (player.transform.position - transform.position).normalized * moveSpeed * Time.deltaTime;
        }
        else if (gameObject.tag == "Projectile")
        {

        }
    }

    void FollowPlayer()
    {

    }

    void FixedUpdate()
    {
    }
}
