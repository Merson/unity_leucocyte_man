﻿using UnityEngine;
using System.Collections;

public class RogueEnemyGenerator : MonoBehaviour {

    public Transform[] enemyPrefabs;
    public float maxEnemy = 10.0f;
    public float enemyRate = 3.0f;

    private float enemyCooldown = 0.0f;
    private float maxEnemyCounter = 0.0f;

    void Start()
    {
        enemyCooldown = 0.0f;
    }

    void Update ()
    {

        if (enemyCooldown > 0)
        {
            enemyCooldown -= Time.deltaTime;
        }

        if (CanSpawn)
        {
            enemyCooldown = enemyRate;
            var enemyTransform = Instantiate(enemyPrefabs[Random.Range(0, enemyPrefabs.Length)]) as Transform;
            enemyTransform.position = transform.position;
            maxEnemyCounter += 1.0f;
            if (maxEnemyCounter == maxEnemy)
            {
                Destroy(gameObject);
            }
        }
   }

    public bool CanSpawn
    {
        get
        {
            return enemyCooldown <= 0f;
        }
    }
}
