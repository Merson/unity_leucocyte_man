﻿using UnityEngine;

public class BoardManager : MonoBehaviour {
    
    public class Count
    {
        public int minimum;
        public int maximum;

        public Count(int min, int max)
        {
            this.minimum = min;
            this.maximum = max;
        }
    }


    public int columns = 10;
    public int rows = 10;
    public GameObject Ground1;
    public GameObject Wall1;
    public GameObject Player1;

    //private Transform boardHolder;

    private void BoardSetup()
    {
        //boardHolder = new GameObject("Board").transform;
        for (int x = -1; x < columns + 1; x++)
        {
            for (int y = -1; y < rows + 1; y++)
            {
                GameObject toInstantiate = Ground1;
                if (x == -1 || x == columns || y == -1 || y == rows)
                    toInstantiate = Wall1;

                GameObject instance = Instantiate(toInstantiate, new Vector3(transform.position.x + (x * 0.64f), transform.position.y + (y * 0.64f), 0f), Quaternion.identity) as GameObject;
                instance.transform.SetParent(transform.GetChild(0));
            }
        }
        GameObject playerInstance = Instantiate(Player1) as GameObject;
    }

    void Start () {
        this.BoardSetup();
	}
	
	void Update () {
	
	}
}
