﻿using UnityEngine;
using System.Collections;

public class RogueShootScript : MonoBehaviour
{
    public bool isPlayer = false;
    public float shootPower = 1.0f;
    public float shootLifeTime = 3.0f;
    public Vector3 shootRotation = new Vector3(0, 0, 0);

    private RogueShootScript shoot;
    private RogueHealthScript health;

    void Start()
    {
        //Invoke("DestroyMyObject", this.shootLifeTime);
        Destroy(gameObject, this.shootLifeTime);
    }

    public void setShoot(bool isPlayer, float shootPower, float shootLifeTime, Vector3 shootRotation)
    {
        this.isPlayer = isPlayer;
        this.shootPower = shootPower;
        this.shootLifeTime = shootLifeTime;
        this.shootRotation = shootRotation;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Wall")
        {
            Destroy(gameObject);
        }
        else if (isPlayer)
        {
            if (collider.gameObject.tag == "Monster")
            {
                //StartCoroutine("ShowDamage", collider);
                Destroy(gameObject);
            }
        }
        else
        {
            if (collider.gameObject.tag == "Player")
                Destroy(gameObject);
        }
    }

    IEnumerator ShowDamage(Collider2D collider)
    {
        Color old = collider.GetComponent<SpriteRenderer>().color;
        collider.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0);
        yield return new WaitForSeconds(0.5f);
        collider.GetComponent<SpriteRenderer>().color = old;
    }

    void Update()
    {
        transform.Rotate(shootRotation);
    }
}

