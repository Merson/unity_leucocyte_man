﻿using UnityEngine;
using System.Collections;

public class RogueAudioHelper : MonoBehaviour {

    public AudioClip[] weaponSounds = new AudioClip[1];
    public AudioClip[] collisionSounds = new AudioClip[1];
    public AudioClip[] deathSounds = new AudioClip[1];
    public AudioClip[] ambianceSounds = new AudioClip[1];

    public float throwSpeed = 2000f;
    public float volLowRange = .5f;
    public float volHighRange = 1.0f;

    private AudioSource audioSource;
    private float vol;

    void Awake() {
        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource = new AudioSource();
        audioSource = GetComponent<AudioSource>();
    }

    void Start () {
        vol = Random.Range(volLowRange, volHighRange);
    }

    void Update () {
    }

    public void test()
    {

    }

    public void PlayWeaponSound()
    {
        if (weaponSounds.Length > 0 && audioSource != null)
            audioSource.PlayOneShot(weaponSounds[Random.Range(0, weaponSounds.Length)], 0.7f);
    }

    public void PlayCollisionSound()
    {
        if (collisionSounds.Length > 0 && audioSource != null)
            audioSource.PlayOneShot(collisionSounds[Random.Range(0, weaponSounds.Length)], 0.7f);
    }

    public void PlayDeathSound()
    {
        if (deathSounds.Length > 0 && audioSource != null)
            audioSource.PlayOneShot(deathSounds[Random.Range(0, deathSounds.Length)], 0.7f);
    }

    public void PlayAmbianceSound()
    {
        if (ambianceSounds.Length > 0)
            audioSource.PlayOneShot(ambianceSounds[Random.Range(0, ambianceSounds.Length)], 0.7f);
    }
}
