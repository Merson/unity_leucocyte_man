﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour
{
    private WeaponScript[] weapons;

    void Awake()
    {
        // Récupération de l'arme au démarrage
        weapons = GetComponentsInChildren<WeaponScript>();
    }

    void Start()
    {
        Destroy(gameObject, 20);
    }

    void Update()
    {
        foreach (WeaponScript weapon in weapons)
        {
            // On fait tirer toutes les armes automatiquement
            if (weapon != null && weapon.CanAttack)
            {
                weapon.Attack(true);
            }
        }
    }
}