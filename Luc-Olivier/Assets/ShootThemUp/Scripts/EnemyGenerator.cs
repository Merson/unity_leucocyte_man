﻿using UnityEngine;
using System.Collections;

public class EnemyGenerator : MonoBehaviour {

    public Transform[] enemyPrefabs;
    public float enemyRate = 2f;

    private float enemyCooldown;

    void Start()
    {
        enemyCooldown = 0f;
    }

    void Update ()
    {

        if (enemyCooldown > 0)
        {
            enemyCooldown -= Time.deltaTime;
        }

        if (CanSpawn)
        {
            enemyCooldown = enemyRate;
            var enemyTransform = Instantiate(enemyPrefabs[Random.Range(0, enemyPrefabs.Length)]) as Transform;
            enemyTransform.position = transform.position;
        }

    }

    public bool CanSpawn
    {
        get
        {
            return enemyCooldown <= 0f;
        }
    }
}
